import React from 'react'

import { StyledButton } from '../styled'

function IconButton({ children }) {
  return <StyledButton>{children}</StyledButton>
}

export default IconButton
