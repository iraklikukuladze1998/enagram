import styled from 'styled-components'

export const StyledImage = styled.img`
  opacity: ${(props) => (props.show ? 1 : 0)};
  transition: opacity 0.5s;
`
