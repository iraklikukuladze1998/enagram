import format from 'date-fns/format'
import isFuture from 'date-fns/isFuture'
import parseISO from 'date-fns/parseISO'

import { loadState, saveState } from '../helpers/persist'
import enagramAPI from './clients/enagram'

function Deferred() {
  var self = this
  this.promise = new Promise(function (resolve, reject) {
    self.reject = reject
    self.resolve = resolve
  })
}

class Karen {
  constructor() {
    this.deltaTime = 0
    this.isAuthenticating = false
    this.isAuthorised = new Deferred()
  }

  waitForGuest(forceFetchToken) {
    return new Promise((resolve) => {
      if (!this.isAuthenticating) {
        this.isAuthenticating = true
        const localData = loadState('enagram_auth')
        if (localData && localData.expires_in) {
          const expireDateOfToken = new Date((localData.expires_in + 10) * 1000)
          if (!isFuture(expireDateOfToken)) {
            this.fetchGuestToken().then((result) => {
              saveState(
                {
                  ...result,
                  expires_in: Math.floor(Date.now() / 1000 + result.expires_in),
                },
                'enagram_auth',
              )
              this.isAuthorised.resolve(true)
              this.isAuthenticating = false
              resolve(true)
            })
            return
          } else {
            if (forceFetchToken) {
              this.fetchGuestToken().then((result) => {
                saveState(
                  {
                    ...result,
                    expires_in: Math.floor(Date.now() / 1000 + result.expires_in),
                  },
                  'enagram_auth',
                )
                this.isAuthorised.resolve(true)
                this.isAuthenticating = false
                resolve(true)
              })
            } else {
              this.isAuthorised.resolve(true)
              this.isAuthenticating = false
              resolve(true)
            }
          }
        } else {
          this.fetchGuestToken().then((result) => {
            saveState(
              {
                ...result,
                expires_in: Math.floor(Date.now() / 1000 + result.expires_in),
              },
              'enagram_auth',
            )
            this.isAuthorised.resolve(true)
            this.isAuthenticating = false
            resolve(true)
          })
        }
      }
    })
  }

  async syncTime(forceFetchToken) {
    const ERROR_401 = 'ERROR_401'
    return new Promise((res, reject) => {
      this.waitForGuest(forceFetchToken)
        .then(async () => {
          try {
            const response = await enagramAPI.get(
              '/applicationinfo/server-time',
              {},
              {},
              { securedRoute: true },
            )
            console.log('server-time is pinged succesfully', response)
            return response
          } catch (e) {
            throw new Error(ERROR_401)
          }
        })
        .catch((error) => {
          if (error.message === ERROR_401) {
            reject(ERROR_401)
          }
        })
        .then((response) => {
          const serverTime = response.data.attributes.dateTime

          const clientDate = new Date()
          const serverDate = parseISO(serverTime)

          this.deltaTime = serverDate.getTime() - clientDate.getTime()
          res(this.deltatime)
        })
        .catch((error) => console.log('qwdqw', error))
    })
  }

  syncedTimestampSync() {
    return Date.now() + this.deltaTime
  }

  async fetchGuestToken() {
    return enagramAPI.post(
      'auth/token',
      {},
      {
        client_id: '7',
        grant_type: 'client_implicit',
      },
    )
  }
  async checkWord() {
    return enagramAPI.post('/SpellChecker/CheckWord', {}, {}, { })
  }

  async getToken() {
    return enagramAPI.get(
      '/Account/GetSystemUserTokens',
      {},
      {},
      {},
    )
  }
  async getRefreshToken() {
    const request = {
      "AccessToken": localStorage.getItem('token'),
      "RefreshToken": localStorage.getItem('refreshToken')
  }
    return enagramAPI.post(
      '/Account/RefreshToken',
      {},
      {},
      {},
    )
  }
}


export default new Karen()
