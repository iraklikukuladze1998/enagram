import api from '@root/shared/api'
import { saveState } from '@root/shared/helpers/persist'
import axios from 'axios'
import qs from 'qs'

import { loadState } from '../../helpers/persist'

export const baseURL = 'https://enagramm.com/API'

async function createRequest(method, url, params = {}, data = {}, options = {}) {
  const headers = {}

  if (url === 'programs') {
    headers['Content-Type'] = null
  }

  if (options.securedRoute) {
    const storage = loadState('enagram_auth') || {}
    headers.Authorization = `Bearer ${storage.access_token}`
  }
  /*
  axios.interceptors.response.use(
    (response) => {
      return response
    },
    (error) => {
      if (error.response.status === 401) {
      }
      return error
    },
  )
  */
  const response = await axios({
    method,
    baseURL,
    url,
    data,
    params,
    headers,
    paramsSerializer: qs.stringify,
    timeout: 10000,
  })
  return response.data
}

const get = (url, params, data, options) => createRequest('get', url, params, data, options)
const post = (url, params, data, options) => createRequest('post', url, params, data, options)
const remove = (url, params, data, options) => createRequest('delete', url, params, data, options)

export default {
  get,
  post,
  remove,
}
