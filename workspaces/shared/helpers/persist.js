export const loadState = (key) => {
  try {
    const serializedState = sessionStorage.getItem(key)
    if (serializedState === null) {
      return undefined
    }
    return JSON.parse(serializedState)
  } catch (err) {
    return undefined
  }
}

export const saveState = (state, key) => {
  try {
    const serializedState = JSON.stringify(state)
    sessionStorage.setItem(key, serializedState)
    return true
  } catch (err) {
    return undefined
  }
}
