const baseConfig = require('@root/shared/webpack/webpack.config.base.js')
const Dotenv = require('dotenv-webpack')

const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const webpack = require('webpack')
const CleanWebpackPlugin = require('clean-webpack-plugin')

const workspacePath = (dir) => path.resolve(__dirname + '/' + dir)

const isDev = process.env.NODE_ENV !== 'production'

const dotenv = require('dotenv').config({
  path: path.resolve(__dirname, '.env'),
})

module.exports = (env) => ({
  ...baseConfig(env),
  entry: {
    index: './entry',
  },
  target: 'web',
  plugins: [
    ...baseConfig(env).plugins,
    new HtmlWebpackPlugin({
      title: 'Loading...',
      filename: 'index.html',
      template: './template/index.html',
      inject: true,
    }),
    new MiniCssExtractPlugin({
      filename: isDev ? '[name].css' : '[name].[hash].css',
      chunkFilename: isDev ? '[id].css' : '[id].[hash].css',
    }),
    new CleanWebpackPlugin(['build/app'], {
      root: path.resolve(__dirname, '../'),
    }),
    new webpack.DefinePlugin({
      'process.env': JSON.stringify(dotenv.parsed),
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
    }),
  ],
  resolve: {
    ...baseConfig(env).resolve,
    modules: [...baseConfig(env).resolve.modules, workspacePath('')],
  },
  devServer: {
    historyApiFallback: {
      disableDotRule: true,
    },
    allowedHosts: ['https://localhost/'],
    // host: 'http://localhost/',
    contentBase: path.resolve(__dirname, 'build'),
    port: 3000,
    open: false,
    compress: true,
    https: true,
  },
})

////checktext
