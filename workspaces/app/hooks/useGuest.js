import api from '@root/shared/api'
import { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'

export const useGuest = () => {
  const [guest, setGuest] = useState()

  let history = useHistory()

  useEffect(() => {
    if (!guest) {
      getGuest()
    }
  }, [guest, history.location.pathname])

  async function getGuest() {
    setGuest(true)
  }

  function deleteGuest() {
    setGuest(null)
  }

  return [guest, deleteGuest]
}
