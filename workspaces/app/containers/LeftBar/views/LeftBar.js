import React, { useEffect, useState, useRef } from "react";
import axios from "axios";
import {t, setLocale} from '@root/app/containers/translate'
import locales from '../../locales'

import { StyledLeftBar,
  StyledActiveAngle ,StyledImg, StyledPageWrapper, StyledItemsTop, StyledItemsBottom,
  StyledNavBarItems, StyledNavBarLabel,StyledBorderWrapper, StyledBorder, StyledSwitcher,
  StyledSwWrapper, StyledChromeIcon,StyledUpgrade, StyledAvatarWrapper,
  StyledbuttonsWrapper,StyledLogos, StyledUserLogin, StyledInputButtons, StyledLanguage, StyledLineBetween,StyledLogo, StyledText, StyledLogoWrapper
}  from '../styled'
import SpellCheck from '../../imgs/icons/SpellCheck'
import HovTop from '../../imgs/icons/HovTop'
import HovBottom from '../../imgs/icons/HovBottom'
import Speech from '../../imgs/icons/Speech'
import TextToSp from '../../imgs/icons/TextToSp'
import Contact from '../../imgs/icons/Contact'
import Fb from '../../imgs/icons/Fb'
import Settings from '../../imgs/icons/Settings'
import Sun from '../../imgs/icons/Sun'
import Moon from '../../imgs/icons/Moon'
import Logo from '../../imgs/icons/Logo'
import Ocr from '../../imgs/icons/Ocr'
import Enagramm from "../../imgs/enagramm.png";
import LogoText from "../../imgs/ennagramText.png";
import logo from '../views/logo.png';
import upgrade from '../views/upgrade.png';


function LeftBar(){
    
  const [defaultLocale, setDefaultLocale ] = useState(localStorage.getItem('locale') ? localStorage.getItem('locale') : 'ka')
 const listOfLocales = ['ka', 'en', 'ru']


let currentLocale = null

function localeKey (strings) {
  let key = ''

  strings.forEach((string, index) => {
    key += string

    if (index < strings.length - 1) {
      key += `{${index + 1}}`
    }
  })

  return key
}

  function t (strings, ...values) {
    const locale = defaultLocale
    const localeStrings = locales.get(locale)
    console.log(locale)
    const key = localeKey(strings)
    let localeString = key
  
    if (typeof localeStrings !== 'undefined') {
      if (typeof localeStrings.get(key) !== 'undefined') {
        localeString = localeStrings.get(key)
      }
    }
  
    if (process.env.NODE_ENV === 'development') {
      reportLocale(key)
    }
  
    values.forEach((value, index) => {
      localeString = localeString.replace(`{${index + 1}}`, value)
    })
  
    return localeString
  }

  useEffect(() => {
    localStorage.setItem('locale', defaultLocale)
   }, [defaultLocale])

  return(
        <StyledLeftBar>
          <div>
           <StyledLogoWrapper>
            <StyledLogo><img src={logo} /></StyledLogo>
            <StyledText>
              <StyledImg ></StyledImg>
            </StyledText>
          </StyledLogoWrapper>
          <StyledItemsTop>
            <StyledUserLogin>
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M12.0078 13.0182C9.41092 13.0182 7.29822 10.9055 7.29822 8.30861C7.29822 5.71173 9.41092 3.59903 12.0078 3.59903C14.6047 3.59903 16.7174 5.71173 16.7174 8.30861C16.7174 10.9055 14.6047 13.0182 12.0078 13.0182Z" fill="white"/>
<path d="M20.4932 3.51469C18.2264 1.24828 15.2132 0 12.0079 0C8.80262 0 5.78903 1.24828 3.52262 3.51469C1.25622 5.78109 0.00793457 8.79469 0.00793457 12C0.00793457 15.2053 1.25622 18.2189 3.52262 20.4853C5.78903 22.7517 8.80262 24 12.0079 24C15.2132 24 18.2264 22.7517 20.4933 20.4853C22.7597 18.2189 24.0079 15.2053 24.0079 12C24.0079 8.79469 22.7597 5.78109 20.4932 3.51469ZM3.24887 18.7205C1.81637 16.8577 0.963247 14.5266 0.963247 12C0.963247 5.91 5.91793 0.955312 12.0079 0.955312C18.0979 0.955312 23.0522 5.91 23.0522 12C23.0522 14.5275 22.199 16.8595 20.7651 18.7228C20.4098 17.5341 19.5051 16.4728 18.1315 15.6614C16.4718 14.6807 14.2869 14.1408 11.9797 14.1408C9.67529 14.1408 7.49983 14.6808 5.85404 15.6623C4.49575 16.4723 3.6009 17.5317 3.24887 18.7205Z" fill="white"/>
</svg>
<span>Sign up</span>
            </StyledUserLogin>
            <StyledNavBarItems active>
              <StyledActiveAngle top>
                <HovTop />
              </StyledActiveAngle>
              <SpellCheck />
              <StyledNavBarLabel>Spellchecker</StyledNavBarLabel>
              <StyledActiveAngle bottom>
                <HovBottom />
              </StyledActiveAngle>
            </StyledNavBarItems>
            <StyledNavBarItems>
              <StyledActiveAngle top>
                <HovTop />
              </StyledActiveAngle>
              <Speech />
              <StyledNavBarLabel>Text to speech</StyledNavBarLabel>
              <StyledActiveAngle bottom>
                <HovBottom />
              </StyledActiveAngle>
            </StyledNavBarItems>
            {/* <StyledNavBarItems disabled>
              <StyledActiveAngle top>
                <HovTop />
              </StyledActiveAngle>
              <TextToSp />
              <StyledNavBarLabel>Speech to text</StyledNavBarLabel>
              <StyledActiveAngle bottom>
                <HovBottom />
              </StyledActiveAngle>
            </StyledNavBarItems>
            <StyledNavBarItems disabled>
              <StyledActiveAngle top>
                <HovTop />
              </StyledActiveAngle>
              <Ocr/>
              <StyledNavBarLabel>OCR</StyledNavBarLabel>
              <StyledActiveAngle bottom>
                <HovBottom />
              </StyledActiveAngle>
            </StyledNavBarItems> */}
          </StyledItemsTop>
          </div>
          <StyledItemsBottom>
            <StyledUpgrade>
              <img src={upgrade} />
              <span>Upgrade to Premium</span>
            </StyledUpgrade>
            <StyledBorderWrapper>
              <StyledBorder></StyledBorder>
            </StyledBorderWrapper>
            <StyledLogos  geo={defaultLocale === 'ka'} chrome={true}>
            <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M20.1666 11C20.1666 16.0618 16.0618 20.1666 10.9999 20.1666C5.93809 20.1666 1.83325 16.0618 1.83325 11C1.83325 5.93815 5.93809 1.83331 10.9999 1.83331C16.0618 1.83331 20.1666 5.93815 20.1666 11Z" fill="#4CAF50"/>
<path d="M10.9999 1.83331V11L14.6666 12.8333L10.6135 20.1666C10.7588 20.1666 10.8546 20.1666 10.9999 20.1666C16.0659 20.1666 20.1666 16.0659 20.1666 11C20.1666 5.93402 16.0659 1.83331 10.9999 1.83331Z" fill="#FFC107"/>
<path d="M20.1666 11C20.1666 16.0618 16.0618 20.1666 10.9999 20.1666C5.93809 20.1666 1.83325 16.0618 1.83325 11C1.83325 5.93815 5.93809 1.83331 10.9999 1.83331C16.0618 1.83331 20.1666 5.93815 20.1666 11Z" fill="#4CAF50"/>
<path d="M10.9999 1.83331V11L14.6666 12.8333L10.6135 20.1666C10.7588 20.1666 10.8546 20.1666 10.9999 20.1666C16.0659 20.1666 20.1666 16.0659 20.1666 11C20.1666 5.93402 16.0659 1.83331 10.9999 1.83331Z" fill="#FFC107"/>
<path d="M19.1766 6.87498H11V12.8333L9.62496 12.375L3.28163 6.07748H3.27246C4.89496 3.52456 7.75038 1.83331 11 1.83331C14.575 1.83331 17.6687 3.88665 19.1766 6.87498Z" fill="#F44336"/>
<path d="M3.28076 6.07935L7.3338 12.8911L9.62501 12.375L3.28076 6.07935Z" fill="#DD2C00"/>
<path d="M10.6135 20.1666L14.7083 12.8063L12.8332 11.4583L10.6135 20.1666Z" fill="#558B2F"/>
<path d="M19.1882 6.875H11.0001L10.2764 8.97417L19.1882 6.875Z" fill="#F9A825"/>
<path d="M15.125 11C15.125 13.2775 13.2775 15.125 11 15.125C8.72254 15.125 6.875 13.2775 6.875 11C6.875 8.72254 8.72254 6.875 11 6.875C13.2775 6.875 15.125 8.72254 15.125 11Z" fill="white"/>
<path d="M14.2084 11C14.2084 12.7724 12.7725 14.2084 11.0001 14.2084C9.22771 14.2084 7.79175 12.7724 7.79175 11C7.79175 9.22765 9.22771 7.79169 11.0001 7.79169C12.7725 7.79169 14.2084 9.22765 14.2084 11Z" fill="#2196F3"/>
</svg>
{t`დაამატე ქრომში`}
            </StyledLogos>
            <StyledLogos  geo={defaultLocale === 'ka'}>
            <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M19.25 2.75H6.41667C5.91021 2.75 5.5 3.16021 5.5 3.66667V6.87638H20.1667V3.66667C20.1667 3.16021 19.7565 2.75 19.25 2.75Z" fill="url(#paint0_linear_31_240)"/>
<path d="M5.5 15.1497V18.3333C5.5 18.8398 5.91021 19.25 6.41667 19.25H19.25C19.7565 19.25 20.1667 18.8398 20.1667 18.3333V15.1497H5.5Z" fill="url(#paint1_linear_31_240)"/>
<path d="M5.5 6.8764H20.1667V11.0023H5.5V6.8764Z" fill="url(#paint2_linear_31_240)"/>
<path d="M5.5 11.0023H20.1667V15.1502H5.5V11.0023Z" fill="url(#paint3_linear_31_240)"/>
<path opacity="0.05" d="M10.2295 5.95831H5.5V16.9583H10.2295C11.1613 16.9583 11.9167 16.203 11.9167 15.2712V7.64544C11.9167 6.71365 11.1613 5.95831 10.2295 5.95831Z" fill="black"/>
<path opacity="0.07" d="M10.181 16.5H5.5V6.11096H10.181C10.9711 6.11096 11.6114 6.75125 11.6114 7.54142V15.0695C11.611 15.8597 10.9707 16.5 10.181 16.5Z" fill="black"/>
<path opacity="0.09" d="M10.1319 16.0417H5.5V6.26404H10.1319C10.78 6.26404 11.3053 6.78929 11.3053 7.43737V14.8683C11.3057 15.5164 10.78 16.0417 10.1319 16.0417Z" fill="black"/>
<path d="M10.0833 15.5834H2.74992C2.24346 15.5834 1.83325 15.1731 1.83325 14.6667V7.33335C1.83325 6.8269 2.24346 6.41669 2.74992 6.41669H10.0833C10.5897 6.41669 10.9999 6.8269 10.9999 7.33335V14.6667C10.9999 15.1731 10.5897 15.5834 10.0833 15.5834Z" fill="url(#paint4_linear_31_240)"/>
<path d="M8.43478 8.70831L7.7262 12.0376L6.94107 8.70831H5.9387L5.12882 12.1408L4.3987 8.70831H3.5022L4.57653 13.2916H5.65453L6.43966 9.76752L7.22524 13.2916H8.25695L9.33128 8.70831H8.43478Z" fill="white"/>
<defs>
<linearGradient id="paint0_linear_31_240" x1="12.8333" y1="6.85942" x2="12.8333" y2="2.95625" gradientUnits="userSpaceOnUse">
<stop stop-color="#42A3F2"/>
<stop offset="1" stop-color="#42A4EB"/>
</linearGradient>
<linearGradient id="paint1_linear_31_240" x1="12.8333" y1="19.25" x2="12.8333" y2="15.1497" gradientUnits="userSpaceOnUse">
<stop stop-color="#11408A"/>
<stop offset="1" stop-color="#103F8F"/>
</linearGradient>
<linearGradient id="paint2_linear_31_240" x1="12.8333" y1="-7.0858" x2="12.8333" y2="-7.11376" gradientUnits="userSpaceOnUse">
<stop stop-color="#3079D6"/>
<stop offset="1" stop-color="#297CD2"/>
</linearGradient>
<linearGradient id="paint3_linear_31_240" x1="5.5" y1="13.0763" x2="20.1667" y2="13.0763" gradientUnits="userSpaceOnUse">
<stop stop-color="#1D59B3"/>
<stop offset="1" stop-color="#195BBC"/>
</linearGradient>
<linearGradient id="paint4_linear_31_240" x1="2.17425" y1="6.75769" x2="10.768" y2="15.351" gradientUnits="userSpaceOnUse">
<stop stop-color="#256AC2"/>
<stop offset="1" stop-color="#1247AD"/>
</linearGradient>
</defs>
</svg>
{t`დაამატე ვორდში`}
            </StyledLogos>
            <StyledBorderWrapper>
              <StyledBorder></StyledBorder>
            </StyledBorderWrapper>
            <StyledNavBarItems >
            <StyledActiveAngle top>
                <HovTop />
              </StyledActiveAngle>

            <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M12.989 2C6.917 2 2 6.928 2 13C2 19.072 6.917 24 12.989 24C19.072 24 24 19.072 24 13C24 6.928 19.072 2 12.989 2ZM20.612 8.6H17.367C17.015 7.225 16.509 5.905 15.849 4.684C17.873 5.377 19.556 6.785 20.612 8.6ZM13 4.244C13.913 5.564 14.628 7.027 15.101 8.6H10.899C11.372 7.027 12.087 5.564 13 4.244ZM4.486 15.2C4.31 14.496 4.2 13.759 4.2 13C4.2 12.241 4.31 11.504 4.486 10.8H8.204C8.116 11.526 8.05 12.252 8.05 13C8.05 13.748 8.116 14.474 8.204 15.2H4.486ZM5.388 17.4H8.633C8.985 18.775 9.491 20.095 10.151 21.316C8.127 20.623 6.444 19.226 5.388 17.4ZM8.633 8.6H5.388C6.444 6.774 8.127 5.377 10.151 4.684C9.491 5.905 8.985 7.225 8.633 8.6ZM13 21.756C12.087 20.436 11.372 18.973 10.899 17.4H15.101C14.628 18.973 13.913 20.436 13 21.756ZM15.574 15.2H10.426C10.327 14.474 10.25 13.748 10.25 13C10.25 12.252 10.327 11.515 10.426 10.8H15.574C15.673 11.515 15.75 12.252 15.75 13C15.75 13.748 15.673 14.474 15.574 15.2ZM15.849 21.316C16.509 20.095 17.015 18.775 17.367 17.4H20.612C19.556 19.215 17.873 20.623 15.849 21.316ZM17.796 15.2C17.884 14.474 17.95 13.748 17.95 13C17.95 12.252 17.884 11.526 17.796 10.8H21.514C21.69 11.504 21.8 12.241 21.8 13C21.8 13.759 21.69 14.496 21.514 15.2H17.796Z" fill="white" stroke="#2D62ED" stroke-width="0.4"/>
</svg>

              <StyledNavBarLabel>Language <svg width="10" height="6" viewBox="0 0 10 6" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M1 1L5 5L9 1" stroke="white" stroke-linecap="round"/>
</svg>
</StyledNavBarLabel>
<StyledActiveAngle bottom>
                <HovBottom />
              </StyledActiveAngle>
            </StyledNavBarItems>
            <StyledNavBarItems >
              <StyledActiveAngle top>
                <HovTop />
              </StyledActiveAngle>
              <Contact />
              <StyledNavBarLabel>Contact support</StyledNavBarLabel>
              <StyledActiveAngle bottom>
                <HovBottom />
              </StyledActiveAngle>
            </StyledNavBarItems>
            <StyledBorderWrapper>
              <StyledBorder></StyledBorder>
            </StyledBorderWrapper>
            <StyledSwitcher>
              <StyledSwWrapper>
                <div><svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M1.73831 0H13.9065C14.8666 0 15.6448 0.77827 15.6448 1.73831V13.9065C15.6448 14.8666 14.8666 15.6448 13.9065 15.6448H1.73831C0.77827 15.6448 0 14.8666 0 13.9065V1.73831C0 0.77827 0.77827 0 1.73831 0ZM11.3129 4.93845V2.91539C11.1034 2.88724 10.3908 2.82471 9.56498 2.82471C7.83271 2.82471 6.65108 3.88158 6.65108 5.82022V7.48996H4.69336V9.75379H6.65108V15.6448H8.98964V9.75379H10.9408L11.2316 7.48996H8.98964V6.04223C8.98964 5.38872 9.171 4.94158 10.1122 4.94158L11.3129 4.93845Z" fill="white"/>
</svg>
</div>
<div>
<svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
<g clip-path="url(#clip0_260_757)">
<path d="M3.4665 16H13.8665C15.4129 16 16.6665 14.7464 16.6665 13.2V2.8C16.6665 1.2536 15.4129 0 13.8665 0L3.4665 0C1.92011 0 0.666504 1.2536 0.666504 2.8V13.2C0.666504 14.7464 1.92011 16 3.4665 16Z" fill="white"/>
<path d="M11.6665 14H5.6665C4.87085 14 4.10779 13.6839 3.54518 13.1213C2.98257 12.5587 2.6665 11.7957 2.6665 11V5C2.6665 4.20435 2.98257 3.44129 3.54518 2.87868C4.10779 2.31607 4.87085 2 5.6665 2H11.6665C12.4622 2 13.2252 2.31607 13.7878 2.87868C14.3504 3.44129 14.6665 4.20435 14.6665 5V11C14.6665 11.7957 14.3504 12.5587 13.7878 13.1213C13.2252 13.6839 12.4622 14 11.6665 14V14ZM5.6665 3.2C5.18932 3.20066 4.73186 3.39052 4.39444 3.72794C4.05702 4.06536 3.86717 4.52281 3.8665 5V11C3.86717 11.4772 4.05702 11.9346 4.39444 12.2721C4.73186 12.6095 5.18932 12.7993 5.6665 12.8H11.6665C12.1437 12.7993 12.6011 12.6095 12.9386 12.2721C13.276 11.9346 13.4658 11.4772 13.4665 11V5C13.4658 4.52281 13.276 4.06536 12.9386 3.72794C12.6011 3.39052 12.1437 3.20066 11.6665 3.2H5.6665Z" fill="#2D62ED"/>
<path d="M8.66648 11.4C7.99402 11.4 7.33667 11.2006 6.77754 10.827C6.21842 10.4534 5.78263 9.9224 5.52529 9.30113C5.26795 8.67986 5.20062 7.99624 5.33181 7.3367C5.463 6.67717 5.78682 6.07134 6.26232 5.59584C6.73782 5.12035 7.34364 4.79653 8.00317 4.66534C8.66271 4.53415 9.34634 4.60148 9.9676 4.85882C10.5889 5.11616 11.1199 5.55194 11.4935 6.11107C11.8671 6.6702 12.0665 7.32755 12.0665 8.00001C12.0658 8.90154 11.7074 9.76596 11.0699 10.4034C10.4324 11.0409 9.56801 11.3993 8.66648 11.4V11.4ZM8.66648 5.80001C8.23136 5.80001 7.80601 5.92904 7.44423 6.17077C7.08244 6.41251 6.80046 6.75611 6.63395 7.1581C6.46743 7.5601 6.42387 8.00245 6.50875 8.42921C6.59364 8.85596 6.80317 9.24797 7.11085 9.55564C7.41852 9.86332 7.81052 10.0728 8.23728 10.1577C8.66404 10.2426 9.10639 10.1991 9.50838 10.0325C9.91038 9.86603 10.254 9.58405 10.4957 9.22226C10.7375 8.86047 10.8665 8.43513 10.8665 8.00001C10.8658 7.41673 10.6338 6.85754 10.2214 6.4451C9.80895 6.03267 9.24975 5.80067 8.66648 5.80001V5.80001Z" fill="#2D62ED"/>
<path d="M12.0666 5.40001C12.3979 5.40001 12.6666 5.13138 12.6666 4.80001C12.6666 4.46864 12.3979 4.20001 12.0666 4.20001C11.7352 4.20001 11.4666 4.46864 11.4666 4.80001C11.4666 5.13138 11.7352 5.40001 12.0666 5.40001Z" fill="#2D62ED"/>
</g>
<defs>
<clipPath id="clip0_260_757">
<rect width="16" height="16" fill="white" transform="translate(0.666504)"/>
</clipPath>
</defs>
</svg>

</div>
<div>
<svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M14.4765 15.6448H2.30826C1.34822 15.6448 0.569946 14.8666 0.569946 13.9065V1.73831C0.569946 0.77827 1.34822 0 2.30826 0H14.4765C15.4365 0 16.2148 0.77827 16.2148 1.73831V13.9065C16.2148 14.8666 15.4365 15.6448 14.4765 15.6448ZM11.7203 13.4719H14.0419V8.70271C14.0419 6.6848 12.898 5.70912 11.3002 5.70912C9.70175 5.70912 9.02903 6.95391 9.02903 6.95391V5.93925H6.79166V13.4719H9.02903V9.51774C9.02903 8.45824 9.51675 7.82776 10.4502 7.82776C11.3083 7.82776 11.7203 8.43361 11.7203 9.51774V13.4719ZM2.74284 3.56291C2.74284 4.33051 3.36039 4.95291 4.12248 4.95291C4.88458 4.95291 5.50176 4.33051 5.50176 3.56291C5.50176 2.7953 4.88458 2.17291 4.12248 2.17291C3.36039 2.17291 2.74284 2.7953 2.74284 3.56291ZM5.3002 13.4719H2.9672V5.93925H5.3002V13.4719Z" fill="white"/>
</svg>

</div>
<div>
<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M14.2616 15.6448H2.09342C1.13337 15.6448 0.355103 14.8666 0.355103 13.9065V1.73831C0.355103 0.77827 1.13337 0 2.09342 0H14.2616C15.2217 0 15.9999 0.77827 15.9999 1.73831V13.9065C15.9999 14.8666 15.2217 15.6448 14.2616 15.6448ZM6.88498 5.80363L6.88537 9.55347L10.2666 7.68501L6.88498 5.80363ZM13.8129 3.82924C14.188 4.23421 14.3104 5.15384 14.3104 5.15384C14.3104 5.15384 14.4356 6.23404 14.4356 7.31382V8.32646C14.4356 9.40666 14.3104 10.4864 14.3104 10.4864C14.3104 10.4864 14.188 11.4061 13.8129 11.811C13.3841 12.2901 12.9086 12.3396 12.6398 12.3676C12.6102 12.3707 12.5832 12.3735 12.559 12.3766C10.8075 12.5117 8.17764 12.5159 8.17764 12.5159C8.17764 12.5159 4.92351 12.4842 3.92224 12.3816C3.8748 12.3721 3.8173 12.3648 3.75217 12.3565C3.43501 12.3159 2.93721 12.2522 2.54198 11.811C2.16689 11.4061 2.04486 10.4864 2.04486 10.4864C2.04486 10.4864 1.9197 9.40666 1.9197 8.32646V7.31382C1.9197 6.23404 2.04486 5.15384 2.04486 5.15384C2.04486 5.15384 2.16689 4.23421 2.54198 3.82924C2.97177 3.3495 3.44771 3.30053 3.7165 3.27288C3.74565 3.26988 3.77235 3.26714 3.7963 3.26412C5.54774 3.12899 8.1749 3.12899 8.1749 3.12899H8.18037C8.18037 3.12899 10.8075 3.12899 12.559 3.26412C12.5829 3.26714 12.6096 3.26989 12.6388 3.27289C12.9074 3.30055 13.3835 3.34957 13.8129 3.82924Z" fill="white"/>
</svg>

</div>
              </StyledSwWrapper>
            </StyledSwitcher>
          </StyledItemsBottom>
        </StyledLeftBar>
  )
}


export default LeftBar
