export default new Map([
  ['მთავარი გვერდი', 'Home'],
  ['არქივი', 'Архив'],
  ['დღის ფილმი', 'Фильм Дня'],
  ['"{1}" - ნაპოვნია: {2}', '{1} результат поиска: {2}'],
  ['ნაპოვნია', 'результат поиска'],
  ['საძიებო სიტყვა', 'Ключевое слово'],
  ['ტრეილერი', 'Трейлер'],
  ['ტრეილერები', 'Трейлеры'],
  ['უყურე', 'Смотреть'],
  ['დღის რეჟიმი', 'Дневной режим'],
  ['რჩეული სერიალი', 'Рекомендуемый сериал'],
  ['პრემიერა', 'Премьера'],
  ['განაგრძე ყურება', 'Продолжить просмотр'],
  ['ვგეგმავ ყურებას', 'Смотреть позже'],
  ['გეგმავს ყურებას', 'Смотреть позже'],
  ['ყველა', 'Все'],
  ['ახალი დამატებული ფილმები', 'Недавно добавленное'],
  ['ახალი ეპიზოდები', 'Новые серии'],
  ['პოპულარული ფილმები', 'Популярные фильмы'],
  ['მაჩვენე მეტი', 'Показать больше'],
  ['იხილეთ ყველა', 'Показать все'],

  ['მომხმარებელს ჯერ არ არქვს შექმნილი სია', 'Нет добавлении'],
  ['შენთვის შემოთავაზებული ფილმები', 'Рекомендуем посмотреть'],
  ['შემოთავაზებული ფილმები', 'Рекомендуем посмотреть'],
  ['რეკლამა IMOVIES-ზე', 'Реклама на iMovies'],
  ['ფასი მოცემულია დღგ-ს ჩათვლით', 'Цена включает НДС'],
  ['ვიდეო რეკლამა', 'Видео Реклама'],
  ['ვიდეო', 'Видео'],
  ['პრეროლი', 'Преролл'],
  ['ვიდეოს დასაწყისში', 'Загружается до начала видео'],
  ['ჩვენება', 'Показов'],
  ['პოზიცია', 'Позиция'],
  ['ხანრგძლივობა', 'Длительность'],
  ['ფასი', 'Цена'],
  ['ბანერი', 'Баннеры'],
  ['ბანერები', 'Баннеры'],
  ['ლარი', 'Лари'],
  ['გვერდი', 'страница'],
  ['ლ', 'Лари'],
  ['ნახვა', 'Пример'],
  ['წამი', 'Секунд'],
  ['კონტაქტი', 'контакт'],
  ['ვიდეო გვერდი', 'Видео на сайте'],
  ['საბანერო რეკლამა', 'Баннер'],
  ['სარეკლამო გვერდი', 'Баннер на сайте'],
  ['სტუდიები', 'Киностудии'],
  ['ტრეილერების გამოწერა', 'Подписаться'],
  ['სიები', 'Подборки'],
  ['ფრანჩიზები', 'Франшизы'],
  ['ფრანჩიზა', 'Франшизы'],
  ['დღეს დაიბადა', 'В этот день родились'],
  ['ფილმოგრაფია', 'Фильмография'],
  ['მსახიობები', 'Персоны'],
  ['ჯარედ ლეტოს ფილმები', 'Фильмы Джареда Лето'],
  ['ფავორიტებში დამატება', 'Добавить в избранное'],
  ['ვგეგმავ ყურებას', 'Смотреть позже'],
  ['ნანახი მაქვს', 'Отметить как просмотренный'],
  ['არ მაქვს ნანახი', 'Не просмотрено'],
  ['გამოწერა', 'Подписаться'],
  ['გამოწერის გაუქმება', 'Отменить подписку'],
  ['გაზიარება', 'Поделиться'],
  ['სიაში დამატება', 'Добавить в подборку'],
  ['წაშლა', 'Удалить'],
  ['ინფო', 'Инфо'],
  ['წავშალოთ ფილმი განაგრძე ყურებიდან', 'Удалить фильм с просмотров?'],
  ['დიახ', 'Да'],
  ['არა', 'Нет'],
  ['მთავარი', 'Главная'],
  ['ფილმები', 'Фильмы'],
  ['სერიალები', 'Сериалы'],
  ['ფესტივალები', 'Фестивали'],
  ['ოსკარები', 'Премия "Оскар"'],
  ['ტელევიზია', 'ТВ'],
  ['მომხმარებლის მენიუ', 'Меню пользователя'],
  ['რჩეულები', 'Избранное'],
  ['ნახვების ისტორია', 'Смотреть историю'],
  ['რჩეული ფრანჩიზები', 'Избранные франшизы'],
  ['ჩემი ფრანჩიზები', 'Подписки франшиз'],
  ['რჩეული სიები', 'Избранные подборки'],
  ['მეტის გაშლა', 'Еще'],
  ['რეკლამა IMOVIES-ზე', 'Реклама на IMOVIES'],
  ['რეკლამა', 'Реклама'],
  ['ძიება', 'Поиск'],
  ['შეტყობინებები', 'Нотификации'],
  ['Desktop/Push შეტყობინებები', 'Desktop/Push нотификации'],
  ['ყველა შეტყობინება', 'Все нотификации'],
  ['თქვენ არ გაქვთ შეტყობინებები', 'У вас нет сообщения'],
  ['პარამეტრები', 'Настройки нотификации'],
  ['მთავარი', 'Главная'],
  ['პერსონები', 'Персоны'],
  ['პერსონა', 'Персона'],
  ['კომენტარები', 'Коментарии'],
  ['კომენტარი', 'Коментарии'],
  ['{1} კომენტარი', 'Коментарии {1}'],
  ['რეიტინგები', 'Реитинги'],
  ['ნახვების ისტორია', 'История просмотров'],
  ['ჩემი ანგარიში', 'Настройки'],
  ['გასვლა', 'Выйти'],
  ['პროფილი', 'Профиль'],
  ['ღამის რეჟიმი', 'Ночной режим'],
  ['ჩემი სიები', 'Мои подборки'],
  ['მომხმარებლის სიები', 'Подборки пользователя'],
  ['TV', 'TV'],
  ['ახალი სიის შექმნა', 'Создать подбороку'],
  ['თქვენ ჯერ არ შეგიქმნიათ სია', 'У вас нет списка подборок'],
  ['მომხმარებელს ჯერ არ აქვს ფილმების სია', 'У пользователя нет списка подборок'],
  ['გამოწერილი სიები', 'Подписки на подборки'],
  ['გამოწერილი ფრანჩიზები', 'Подписки на франшизы'],
  ['თქვნ არ გაქვთ დამატებული ფრანჩიზები', 'У вас нет созданных франшиз'],
  ['მომხმარებელს არ აქვს დამატებული ფრანჩიზები', 'У пользователя нет созданных франшиз'],
  ['თქვენ ჯერ არ გაქვთ დამატებული ფილმი “რჩეულები”-ს ბლოკში.', 'Нет добавлении'],
  ['მომხმარებელს ჯერ არ აქვს დამატებული ფილმი “რჩეულები”-ს ბლოკში.', 'Нет добавлении'],
  ['თქვენ ჯერ არ გაქვთ დამატებული მსახიობები და რეჟისორები “პერსონები”-ს ბლოკში.', 'Нет добавлении'],
  ['მომხმარებელს ჯერ არ აქვს დამატებული მსახიობები და რეჟისორები “პერსონები”-ს ბლოკში.', 'Нет добавлении'],
  ['თქვენ ჯერ არ გაქვთ დამატებული ფილმი “რეიტინგები”-ს ბლოკში.', 'Нет добавлении'],
  ['მომხმარებელს ჯერ არ აქვს დამატებული მსახიობები და რეჟისორები “პერსონები”-ს ბლოკში.', 'Нет добавлении'],
  ['მომხმარებელს ჯერ არ აქვს გამოქვეყნებული კომენტარი', 'Нет добавлении'],
  ['თქვენ ჯერ არ გაქვთ გამოქვეყნებული კომენტარი', 'Нет добавле'],
  ['დაწერე პირველი კომენტარი', 'Добавить комментарий'],
  ['ნამდვილად გსურთ კომენტარის წაშლა', 'Вы деиствтвительно хотите стереть комментарий'],
  ['უპასუხე', 'Ответить'],
  ['ინკოგნიტო', 'Инкогнито'],
  ['გამოქვეყნება', 'Отправить'],
  ['მეტი', 'Еще'],
  ['ფილმი მალე დაემატება', 'Фильм скоро будет добавлен'],
  ['გამარჯვებული', 'Победитель'],
  ['ნომინირებული', 'Номинирован'],
  ['გამარჯ.', 'Победитель'],
  ['ნომინ.', 'Номинирован'],
  ['აღარ ვგეგმავ ყურებას', 'Не планирую смотреть'],
  ['ფავორიტებიდან ამოღება', 'Удалить из избранных'],
  ['რეჟისორი', 'Режисер'],
  ['მომხმარებლის რეიტინგი', 'Рейтинг по отзывам пользователей'],
  ['ფილმის დასახელება სხვადასხვა ენებზე', 'Название фильма на разных языках'],
  ['ალტერნატიული სახელები', 'Альтернативное название'],
  ['წელი', 'Год выпуска'],
  ['ხანგრძლივობა', 'Продолжительность'],
  ['წუთი', 'Минут'],
  ['ქვეყანა', 'Страна'],
  ['სტუდია', 'Студия'],
  ['სტუდიები', 'Студии'],
  ['ჟანრი', 'Жанр'],
  ['მსგავსი', 'Похожее'],
  ['ეს სერიალი სიებში', 'Этот сериал в списках подборок'],
  ['ეს ფილმი სიებში', 'Этот фильм в списках подборок'],
  ['პრობლემა', 'Сообщить о проблеме'],
  ['სუბტიტრები არ ემთხვევა გახმოვანებას', 'Субтитры не совпадают со звуком'],
  ['ცუდი გახმოვანება', 'Плохая озвучка'],
  ['ვიდეოს ცუდი ხარისხი', 'Видео не воспроизводится'],
  ['ვიდეო არ ირთვება', 'Видео не запускается'],
  ['გაგზავნა', 'Отправить'],
  ['აირჩიეთ ხარვეზის ტიპი', 'Выбрать тип проблемы'],
  ['ხარვეზის შეტყობინება', 'Текст сообщения'],
  ['აირჩიეთ სეზონი, სერია', 'Выбрать сезон, эпизод'],
  ['აირჩიეთ სეზონი', 'Выбрать сезон'],
  ['აირჩიეთ სერია', 'Выбрать эпизод'],
  ['სეზონი', 'Сезон'],
  ['სერია', 'Эпизод'],
  ['სიის სახელი', 'Название подборки'],
  ['გაუქმება', 'Отменить'],
  ['შექმნა', 'Создать'],
  ['თქვენ ჯერ არ გაქვთ სიები', 'Нет добавлении'],
  ['რეიტინგი', 'IMDB рейтинг'],
  ['მუქი თემა', 'Темная тема'],
  ['თვქენს მიერ განხორციელებული ცვლილებები დამახსოვრებულია', 'Изменения сохранены.'],
  ['ცვლილებების დამახსოვრება ', 'Сохранить изменения'],
  ['პირადი ინფორმაცია', 'Личные данные'],
  ['ჩემი სახელი და გვარი', 'Имя, фамилия'],
  ['პროფილის სახელი (username)', 'Имя профиля'],
  ['დაბადების თარიღი', 'Дата рождения'],
  ['დღე', 'День'],
  ['თვე', 'Месяц'],
  ['პრივატულობა', 'Приватность'],
  ['ღია პროფილი', 'Открытый профиль'],
  ['ფარული პროფილი ხელმისაწვდომია მხოლოდ იმ მომხმარებლებისთვის, რომელთაც თქვენ მისცემთ ავტორიზაციას. ყველა დანარჩენი - მხოლოდ თქვენს სახელს და სურათს დაინახავს.', 'uufff'],
  ['ნებისმიერი სოციალური ქმედება, მაშინაც კი, როდესაც თქვენი ანგარიში ფარულია, აისახება საიტზე. მაგალითად, თუ თქვენ გახდებით რომელიმე ფილმის ფანი, თქვენი სახელი დაემატება ამ ფილმის ფანების სიას.', 'fffffff'],
  ['შენიშვნა', 'Предупреждение'],
  ['ჩემ შესახებ', 'Обо мне'],
  ['ჩემი ელ. ფოსტა', 'Эл. почта'],
  ['ფილტრი', 'Фильтр'],
  ['ჟანრების გაერთიანება', 'Комбинация жанров'],
  ['გახმოვანება', 'Озвучка'],
  ['IMDB რეიტინგი', 'IMDB рейтинг'],
  ['iMOVIES რეიტინგი', 'iMOVIES рейтинг'],
  ['ქვეყნების გაერთიანება', 'Комбинация стан'],
  ['გასუფთავება', 'Очистить фильтр'],
  ['მითითებული პარამეტრებით ფილმი ვერ მოიძებნა', 'По указанным параметрам результаты не найдены'],
  ['ნანახი ფილმების დამალვა', 'Скрыть просмотренные фильмы'],
  ['ნანახი ფილმების გამოჩენა', 'Показать просмотренные фильмы'],
  ['სუბტიტრებით', 'С субтитрами'],
  ['სუბტიტრების გარეშე', 'Без субтитров'],
  ['ფილტრაცია', 'Фильтрация'],
  ['ტიპი', 'Тип'],
  ['ყველა', 'Все'],
  ['ფილმი', 'Фильм'],
  ['სერიალი', 'Cериалы'],
  ['და მეტი', 'и больше'],
  ['-დან', 'C -'],
  ['-მდე', 'До -'],
  ['გაფილტვრა', 'Фильтрация'],
  ['"და" ფილტრის ჩართვა', 'Включить множественный выбор'],
  ['ფილტრების გასუფთავება', 'Очистить фильтр'],
  ['{1} -ზე მეტი ხმით', 'Больше {1} голосов'],
  ['გამოწერილია', 'Подписан(а)'],
  ['მიმდევარი', 'Подписчик'],
  ['მიმდევრები', 'Подписчики'],
  ['სია', 'Подпорка'],
  ['ფილმი დამატებულია ნანახად', 'Фильм отмечен как просмотренный'],
  ['ფილმი წაშლილია ნანახებიდან', 'Фильм удален из списка просмотренных'],
  ['ტრეილერები უკვე გამოწერილი გაქვთ', 'Вы уже подписаны на трейлеры'],
  ['გამოწერა გაუქმებულია', 'Подписка отменена'],
  ['ფილმი უკვე გამოწერილია', 'Вы уже подписаны на этот фильм'],
  ['ფილმი დაგეგმილია საყურებლად', 'Фильм добавлен в список "смотреть позже"'],
  ['გეგმები ჩაშლილია', 'Фильм удален из спискa "смотреть позже"'],
  ['დამატებულია ფავორიტებში', 'Добавлен в избранное'],
  ['ამოღებულია ფავორიტებიდან', 'Удален из избранных'],
  ['შეტყობინება წარმატებით გაიგზავნა', 'Сообщение отправлено'],
  ['ამოღებულია სიიდან', 'Фильм удален из подборок'],
  ['დამატებულია სიაში', 'Добавлен в подборку'],
  ['უყურე ფილმს', 'Смотреть фильм'],
  ['როლებში', 'В ролях'],
  ['გამოსვლის თარიღი', 'Дата выхода'],
  ['გაიგე მეტი', 'Узнать больше'],
  ['სიმაღლე', 'Рост'],
  ['ჰოროსკოპი', 'Гороскоп'],
  ['გარდაცვალება', 'Смерть'],
  ['წლის', ''],
  ['დაბადება', 'Рождение'],
  ['ასაკი', 'Возраст'],
  ['ფანი', 'Подписчик'],
  ['ფანები', 'Подписчики'],
  ['სრული ფილმოგრაფია', 'Фильмы'],
  ['დამატების თარიღით', 'Дата добавления'],
  ['გამოშვების წელი', 'Дата выхода'],
  ['iMovies რეიტინგით', 'iMovies рейтинг'],
  ['IMDB რეიტინგით', 'IMDB рейтинг'],
  ['{1} {2}', '{2} {1}'],
  ['ჯერჯერობით არ ჰყავს ფანები', 'Нет добавлении'],
  ['ჯერჯერობით არ ჰყავს გამომწერები', 'Нет добавлении'],
  ['უკან', 'Назад'],
  ['წინ', 'Вперед'],
  ['პირველი', 'Первый'],
  ['ბოლო', 'Последний'],
  ['შეტყობინებების პარამეტრები', 'Настройки нотификации'],
  ['შეტყობინება', 'Нотификации'],
  ['მსახიობი', 'Персоны'],
  ['მოძებნე და უყურე საუკეთესო ფილმებს ონლაინ რეჟიმში გადმოწერის გარეშე', 'Смотреть лучшие фильмы онлайн без скачивания'],
  ['ოსკაროსანი', 'Номинант премии "Оскар"'],
  ['ოსკარზე ნომინირებული', 'Обладатель Оскара'],
  ['კომენტარი გამოქვეყნებულია', 'Комментарий опубликован'],
  ['აღწერა', 'Описание'],
  ['თქვენი რეიტინგი დაფიქსირებულია', 'Ваш голос получен'],
  ['კომენტარსაც ხომ არ დაუწერდი', 'Хотите сказать больше'],
  ['დაგვიწერეთ, რა შთაბეჭდილებები დაგიტოვათ ფილმმა და ვის ურჩევდით ამ ფილმის ნახვას', 'Оставьте комментарий или рецензию на фильм'],
  ['სიის რედაქტირება', 'Редактирование'],
  ['ეს სია დახურლია', 'Эта частная подборка'],
  ['ჯერჯერობით ამ სიაში არ არის დამატებული ფილმები', 'Нет добавлении'],
  ['გამომწერები', 'Подписчики'],
  ['სამწუხაროდ, გვერდი არ მოიძებნა', 'Страница не найдена'],
  ['იყავი პირველი, გამოხატე შენი აზრი', 'Оставьте коментарий'],
  ['ფილმი: ნაწილი {1}', 'Фильм: часть {1}'],
  ['მალე', 'Скоро'],
  ['მომხმარებლის შეფასება', 'Оценка пользователя'],
  ['ნომინაცია', 'Номинация'],
  ['ნამდვილად გსურთ წაშლა', 'Вы действительно хотите удалить'],
  ['ფესტივალი', 'Фестиваль'],
  ['ისტორიიდან წაშლა', 'Удалить из историй просмотров'],
  ['ნომინაციები', 'Номинации'],
  ['დღის', 'Сегодня'],
  ['კვირის', 'За неделю'],
  ['თვის', 'За месяц'],
  ['სულ', 'Все'],
  ['ავტორიზაცია', 'Авторизация'],
  ['პაროლის აღსადგენად გთხოვთ შეიყვანოთ ელ-ფოსტა', 'Для восстановления пароля введите вашу электронную почту'],
  ['პოპულარული სერიალები', 'Избранные сериалы'],
  ['ნანახი', 'Просмотренные'],
  ['სმ', 'cm'],
  ['პრემიერების არქივი', 'ПРЕМЬЕРЫ'],
  ['დღის ფილმების არქივი', 'Фильмы дня'],
  ['რეზულტატი არ მოიძებნა', 'Результаты не найдены'],
  ['დამახსოვრება', 'Запомнить'],
  ['გაიმეორე პაროლი', 'Повторите пароль'],
  ['პაროლი უნდა შეიცავდეს, მინიმუმ 8 სიმბოლოს', 'Пароль должен содержать минимум 8 символов'],
  ['სესიას გაუვიდა ვადა, სცადეთ ხელახლა', 'Сессия устарела. Повторите войти в систему заново'],
  ['პაროლი', 'Пароль'],
  ['პაროლის აღდგენა წარმატებით განხორციელდა, შეგიძლიათ', 'Пароль был успешно изменен'],
  ['გაიაროთ ავტორიზაცია', 'Войти в систему'],
  ['პაროლები ერთმანეთს არ ემთხვევა', 'Пароли не совпадают'],
  ['-ით ავტორიზაცია შეუძლებელია.თქვენს პროფილში მოსახვედრად გთხოვთ, გაიაროთ რეგისტრაცია და გამოიყენოთ ელ-ფოსტა, რომელიც მითითებული გაქვთ თქვენი', 'авторизация временно недоступна. Восстановите пароль, чтобы войти в систему. Пожалуйста, используйте электронную почту, указанную в вашем'],
  ['პაროლის აღდგენა', 'Восстановить пароль'],
  ['-ის ანგარიშში.', 'аккаунте'],
  ['დაელოდეთ', 'Загрузка'],
  ['დროებით', ''],
  ['-საიტზე შეიცვალა რეგისტრაციის ფორმა.', '-Изменилась форма регистрации на сайте.'],
  ['-თქვენს პროფილში მოსახვედრად საჭიროა გაიაროთ განახლებული რეგისტრაცია.', '-Для авторизации на сайте, пожалуйста, пройдите заново регистрацию.'],
  ['-რეგისტრაციი დროს მიუთითეთ ელ-ფოსტა, რომელიც გამოყენებული გაქვთ თქვენი Facebook -ის ანგარიშში.', 'Пожалуйста, используйте электронную почту, указанную в вашем Facebook аккаунте'],
  ['სახელი', 'Пользователь'],
  ['პაროლის აღსადგენი ბმული გამოიგზავნება უახლოეს 5 წუთში. გთხოვთ, შეამოწმოთ ელ-ფოსტა.', 'В течении 5 минут на указанный адрес вы получите письмо со ссылкой на подтверждение электронной почты. Пожалуйста, проверьте электронную почту.'],
  ['მომხმარებელი მსგავსი ელ-ფოსტით არ მოიძებნა', 'Пользователь с таким электронным адресом не существует.'],
  ['რეგისტრაცია', 'Регистрация'],
  ['ელ-ფოსტა', 'Электронная почта'],
  ['მსგავსი პაროლით და ელ-ფოსტით მომხმარებელი ვერ მოიძებნა', 'Пароль или пользователь с таким именем не найдены.'],
  ['შეიყვანეთ ვალიდური ელ-ფოსტა', 'Пожалуйста, введите существующую  электронную почту'],
  ['მომხმარებელი უკვე არსებობს,', 'Пользователь с таким именем уже существует.'],
  ['გთხოვთ სცადოთ ხელახლა', 'Пожалуйста, повторите заново'],
  ['თქვენ წარმატებით გაიარეთ რეგისტრაცია, გთხოვთ დაადასტუროთ თქვენს ელ-ფოსტა და ამის შემდეგ გაიარეთ', 'Вы успешно прошли регистрацию.  Чтобы войти в систему, пожалуйста, подтвердите ссылку, отправленную на вашу электронную почту Пароли не совпадают'],
  ['შეავსეთ ყველა ველი', 'Пожалуйста,  заполните все обязательные поля'],
  ['საშინელი', 'Отвратительный'],
  ['ცუდი', 'Плохой'],
  ['შეამოწმეთ ელ-ფოსტა და დაადასტურეთ', 'Подтвердите ссылку, отправленную на вашу электронную почту.'],
  ['ნორმალური', 'Нормальный'],
  ['კარგი', 'Хороший'],
  ['გაიარეთ ავტორიზაცია', 'Войти'],
  ['შესანიშნავი', 'Отличный'],
  ['მომწონს', 'Нравится'],
  ['შენახვა', 'Сохранить'],
  ['ღია სია', 'Откытая подборка'],
  ['მონაცემი არ მოიძებნა', 'Информация не найдена'],
  ['გთხოვთ, შეავსოთ სავალდებულო ველი', 'Поле обязательно для заполнения'],
  ['გთხოვთ, შეავსოთ სავალდებულო ველი (მინიმუმ 2 სიმბოლო)', 'Поле обязательно для заполнения (мин. 2 символа)'],
  ['აღწერა არ უნდა შეიცავდეს მხოლოდ 1 სიმბოლოს', 'Описание должно состоять мин. из двух символов'],
  ['სიის აღწერა', 'Описание'],
  ['ნამდვილად გსურთ სიის წაშლა', 'Вы действительно хотите удалить подборку'],
  ['გააქტიურება', 'Активация'],
  ['საიტის შიდა შეტყობინებები', 'Push нотификации'],
  ['შეტყობინებები ელ.ფოსტით', 'Email нотификации'],
  ['ფილმის განთავსება', 'Новые фильмы'],
  ['ახალი ეპიზოდების განთავსება (მალე)', 'Новые серии (скоро)'],
  ['ტრეილერების განთავსება', 'Новые трейлеры'],
  ['პროფილის განახლებები', 'Обновления профиля'],
  ['დასტური', 'Сохранить'],
  ['სინქრონიზაცია წარმატებით დასრულდა', 'Синхронизация успешно завершена'],
  ['კარგი, გადამიყვანე საიტზე', 'Закрыть'],
  ['ნახვების ისტორიის სინქრონიზაცია', 'Синхронизация историй просмотров'],
  ['ნახვების ისტორიის სინქრონიზაცია მოგცემთ საშუალებას, ავტორიზაციის გავლის შემდეგ, ნებისმიერ მოწყობილობაზე გააგრძელოთ ფილმების ყურება', 'Синхронизация дает возможность, после авторизации, продолжить смотреть фильмы на всех устройствах'],
  ['გაუქმების შემთხვევაში, ფილმები, რომელსაც ამჟამად უყურებთ, წაიშლება “განაგრძე ყურების” ბლოკიდან', 'В случае отмены все фильмы, которые вы в данные момент смотрите, будут удалены с истории просмотров'],
  ['გაუქმება', 'Отмена'],
  ['სინქრონიზაცია', 'Синхронизация'],
  ['ნამდვილად გსურთ გაუქმება', 'Вы действительно хотите отменить синхронизацию'],
  ['iMOVIES აპლიკაცია Android-სთვის', 'iMOVIES для Андроид'],
  ['უყურე 5,000-მდე ქართულად გახმოვანებულ ფილმს და სერიალს. ასევე, ისარგებლე ბევრი სხვა საინტერესო ფუნქციით', 'Смотрите более 5000 фильмов и сералов онлайн'],
  ['გადმოწერა', 'Скачать'],
  ['არა, გმადლობთ', 'Нет, спасибо'],
  ['დღეს', 'Cегодня'],
  ['გუშინ', 'Вчера'],
  ['გუშინწინ', 'Позавчера'],
  ['გამომწერი', 'Подписки'],
  ['პოპულარული', 'Популярные'],
  ['სიები რომელშიც ნაპოვნია {1}: {2}', 'Подбороки содержащие "{1}": {2}'],
  [', რომელშიც ნაპოვნია', ', содержащие'],
  ['ყველას ნახვა', 'Больше'],
  ['თქვენი შეფასება მიღებულია', 'Ваш голос принят'],
  ['სამომხმარებლო შეთანხმება', 'Пользовательское соглашение'],
  ['ჰელოუინი', 'Хэллоуин'],
  ['ჟანრები', 'Жанры'],
  ['დღეს დაიბადნენ', 'Сегодня родились'],
  ['მომავალი ეპიზოდები', 'Следующие серии'],
  ['კონფიდენციალურობის პოლიტიკა', 'Политика Конфиденциальности'],
  ['აღნიშნული სამომხმარებლო შეთანხმება აწესრიგებს ურთიერთობას imovies.cc სერვისის ადმინისტრაციასა და სერვისის მომხმარებლებს შორის.', 'The user agreement provided below regulates the relationships between the imovies.cc service administration and the users of the service.'],
  ['სამომხმარებლო შეთანხმების ყველა პუნქტი განკუთვნილია imovies.cc -ის ყველა მომხმარებლისათვის.', 'Each and every point of the user agreement is meant for every user of imovies.cc.'],
  ['imovies.cc -ს ნებისმიერი სერვისის მოხმარებისას თქვენ ავტომატურად ეთანხმებით ქვემოთ მოცემული სამომხმარებლო შეთანხმების ყველა პუნქტს, წინააღმდეგ შემთხვევაში გთხოვთ, ნუ გამოიყენებთ imovies.cc  სერვისს.', 'While using any service offered by adjaranet.ge, you agree to each and every piece of the agreement. Otherwise, please do not use imovies.cc service.'],
  ['მიუხედავად იმისა, რომ imovies.cc  -ის ადმინისტრაცია ეცდება აცნობოს მოხმარებლებს შეთანხმებაში შესული მნიშვნელოვანი ცვლილებების შესახებ, სამომხმარებლო შეთანხმება შესაძლოა შეიცვალოს ადმინისტრაციის მიერ ყოველგვარი სპეციალური შეტყობინების გარეშე. შესაბამისად, სერვისის გამოყენებისას იხელმძღვანელეთ სამომხმარებლო შეთანხმების უახლესი ვერსიით, რომელიც ხელმისაწვდომია ყველა მოხმარებლისათვის შემდეგ ბმულზე სამომხმარებლო შეთანხმება .', 'Despite the fact that adjaranet.GE administration tries hard to notify the users about the changes made to the user agreement, there may still be the cases that the agreement is altered without any notice. So please use the most up to date version of the user agreement while using the service, which is available for every user at: user agreement .'],
  ['დაუშვებელია სხვა მომხმარებლის ანგარიშის ნებართვის გარეშე გამოყენება. მომხმარებელმა აუცილებლად უნდა აცნობოს imovies.cc - ის ადმინსიტრაციას მისი ანგარიშის სხვა პირის მიერ უნებართვოდ გამოყენების შემთხვევაში.', 'You may never use another`s account without permission. You must notify adjaranet.com administration immediately of any breach of security or unauthorized use of your account.'],
  ['სამომხმარებლო შეთანხმების თაობაზე ყველა შესაძლო დავა გადაიჭრება საქართველოს საკანონმდებლო ნორმების თანახმად.', 'Any kind of dispute concerning the user agreement will be solved according to the Georgian legislation.'],
  ['imovies.cc შესაძლოა შეიცავდეს ბმულებს, რომლებიც მომხმარებლებს მესამე პირთა ვებსაიტებზე გადაამისამართებს. imovies.cc არ არის პასუხისმგებელი ამგვარი ვებსაიტების შინაარსზე, მათ მიერ შემოთავაზებულ სერვისსა და მათ სამოხმარებლო შეთანხმებაზე. შესაბამისად, მომხმარებლის ვალდებულებაა საიტის დატოვებისას ყურადღება მიაქციოს და გაეცნოს მესამე მხარის სამომხმარებლო შეთანხმებასა და მოხმარების წესებს.', 'imovies.cc may contain the external links which redirect the users to the third-party websites adjaranet.com is not responsible for the content and service offered by these websites, as well as the user agreement they guide with. So, it is user’s responsibility to be cautious while leaving adjaranet.com and be aware of the rules and privacy policy of the third-party websites.'],
  ['იმისათვის რომ ისარგებლოთ imovies.cc ის მიერ შემოთავაზებული სხვადასხვა სერვისით, მოხმარებელმა უნდა შექმნას საკუთარი ანგარიში. რეგისტრაციისას მომხმარებელმა უნდა მიუთითოს ზუსტი და სრული ინფორმაცია.', 'For being able to use the services offered by adjaranet.com, the user should create her own account. While registration, the user should provide accurate and complete information'],
  ['იმ შემთხვევაში, თუ მომხმარებელი ბოლო ავტორიზაციის გავლიდან 1 წლის განმავლობაში არ გაივლის ავტორიზაციას imovies.cc - ზე, ადმინისტრაცია უფლებას იტოვებს გააუქმოს აღნიშნული მომხმარებლის ანგარიში.', 'If, a user does not log in on adjaranet.com for a year after the last authorization, the administration as the right to deactivate the user’s account.'],
  ['საქართველოს ხელისუფლების მიერ ისეთი ნორმატიულ-საკანონმდებლო აქტების მიღების შემთხვევაში, რომლებიც მთლიანად ან ნაწილობრივ შეეხება საიტის ფუნქციონირებას, ადმინისტრაცია იტოვებს უფლებას ნებისმიერი ცვლილებები შეიტანოს საიტის ფუნქციონირებასა და სამოხმარებლო შეთანხმებაში, რათა ეს უკანასკნელი ახალ ნორმებს მიუსადაგოს.', 'In case of the changes to the normative or legislative acts made by the Georgian government concerning the functionality of the website directly or partially, the administration has the right to make necessary changes in website functionality in order to match it with the new legislation.'],
  ['როგორ უნდა მოიქცეს მომხმარებელი თუ სურს რომ წაიშალოს მის შესახებ არსებული ინფორმაცია.თუ მომხმარებელს სურს რომ წაიშალოს მისი პროფილი და მთელი ის იმფორმაცია რომელსაც imovies.cc ფლობს მის შესახებ, შეუძლია მოიწეროს  support - თან, მეილზე ან fb-ს საშუალებით. განაცხადის გაკეთებიდან უახლოეს პერიოდში ჩვენ წავშლით მთელს ინფორმაციას რაც ამ მომხმარებელთანაა დაკავშირებული.', 'How the user may request that the data be deleted.If a user wants to delete his / her profile and all the information that imovies.cc contains about him / her, he / she can contact to support, using  email or facebook. In the near future, we will delete all information related to this user.']
])
