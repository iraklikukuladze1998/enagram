import en from './en'
import ru from './ru'

export default new Map([
  ['ru', ru],
  ['en', en]
])
