import { ToolTip } from '@root/shared/components/ToolTip'
import {
  Home,
  Movie,
  Radio,
  Settings,
  Sport,
  TVShow,
  User,
} from '@root/shared/icons/header'
import React, { useState } from 'react'

import {
  StyledHeader,
  StyledHeaderSectionWrapper,
  StyledLogoWrapper,
  StyledMenuIcon,
  StyledMenuIconsWrapper,
} from '../styled'

const menuItems = [
  { id: 'home', Icon: () => <Home />, text: 'მთავარი' },
  { id: 'tvShow', Icon: () => <TVShow />, text: 'ვიდეო' },
  { id: 'radio', Icon: () => <Radio />, text: 'რადიო' },
  { id: 'movie', Icon: () => <Movie />, text: 'კინოთეატრი' },
  { id: 'sport', Icon: () => <Sport />, text: 'სპორტი' },
]

function ToolTipWrapper({ children, active, onClick, content }) {
  return (
    <StyledMenuIcon onClick={onClick} active={active} to={`/`}>
      <ToolTip direction={'right'} content={content}>
        {children}
      </ToolTip>
    </StyledMenuIcon>
  )
}

function Header() {
  const [activeTab, setActiveTab] = useState('tvProgram')
  return (
    <StyledHeader>
      <StyledHeaderSectionWrapper>
        <StyledLogoWrapper>
        </StyledLogoWrapper>
        <StyledMenuIconsWrapper>
          {menuItems.map((item) => (
            <ToolTipWrapper
              onClick={() => setActiveTab(item.id)}
              active={activeTab === item.id}
              key={item.id}
              content={item.text}
            >
              <item.Icon />
            </ToolTipWrapper>
          ))}
        </StyledMenuIconsWrapper>
      </StyledHeaderSectionWrapper>
      <StyledHeaderSectionWrapper>
        <ToolTipWrapper content={'პროფილი'}>
          <User />
        </ToolTipWrapper>
        <ToolTipWrapper content={'პარამეტრები'}>
          <Settings />
        </ToolTipWrapper>
      </StyledHeaderSectionWrapper>
    </StyledHeader>
  )
}

export default Header
