import $ from 'jquery';
import Config from "./config.json";

const serviceUrl = '/SpellChecker';

export class SpellCheckerService {

  getSpellCheckerLanguages = (toolsLangGlobalID, webLanguege, accessToken) => {
    return new Promise(function(resolve, reject) {
      $.ajax({
        type: 'GET',
        url: 'https://enagramm.com' +'/API/SpellChecker' + '/GetLanguages?ToolsLangGlobalID='+ toolsLangGlobalID,
        dataType: 'json',
        async: false,
        contentType: "application/json; charset=utf-8",
        headers: {'Authorization': 'Bearer ' + accessToken, 'Accept-Language': webLanguege},
        success: function(data) {
          resolve(data) 
        },
        error: function(err) {
          reject(err) 
        }
      });
    });
  }

  getDictionary = (accessToken, toolsLangGlobalID) => {
    return new Promise(function(resolve, reject) {
      $.ajax({
        type: 'GET',
        url: 'https://enagramm.com' + '/Tools'+ '/GetDictionary?ToolsLangCode='+ toolsLangGlobalID,
        dataType: 'json',
        async: false,
        contentType: "application/json; charset=utf-8",
        headers: {"Authorization": 'Bearer ' + accessToken},
        success: function(data) {
          resolve(data) 
        },
        error: function(err) {
          reject(err) 
        }
      });
    });
  }

  addToDictionary = (accessToken, request, webLanguege) => {
    return new Promise(function(resolve, reject) {
      $.ajax({
        type: 'POST',
        url: 'https://enagramm.com' + '/Tools' + '/AddToDictionary',
        dataType: 'json',
        async: false,
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(request),
        headers: {"Authorization": 'Bearer ' + accessToken, 'Accept-Language': webLanguege},
        success: function(data) {
          resolve(data) 
        },
        error: function(err) {
          reject(err) 
        }
      });
    });
  }

  removeFromDictionary = (accessToken, request, webLanguege) => {
    return new Promise(function(resolve, reject) {
      $.ajax({
        type: 'DELETE',
        url: Config.API_URL + serviceUrl + '/RemoveFromDictionary',
        dataType: 'json',
        async: false,
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(request),
        headers: {"Authorization": 'Bearer ' + accessToken, 'Accept-Language': webLanguege},
        success: function(data) {
          resolve(data) 
        },
        error: function(err) {
          reject(err) 
        }
      });
    });
  }

  checkWord = (accessToken, request, webLanguege) => {
    return new Promise(function(resolve, reject) {
      $.ajax({
        type: 'POST',
        url: Config.API_URL + serviceUrl + '/CheckText',
        dataType: 'json',
        async: false,
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(request),
        headers: {"Authorization": 'Bearer ' + accessToken, 'Accept-Language': webLanguege},
        success: function(data) {
          resolve(data) 
        },
        error: function(err) {
          reject(err) 
        }
      });
    });
  }

  getSuggestions = (accessToken ,word, toolsLangCode) => {
    return new Promise(function(resolve, reject) {
      $.ajax({
        type: 'GET',
        url: Config.API_URL + serviceUrl + '/GetSuggestions?Word='+ word +'&ToolsLangCode='+ toolsLangCode,
        dataType: 'json',
        async: false,
        contentType: "application/json; charset=utf-8",
        headers: {"Authorization": 'Bearer ' + accessToken},
        success: function(data) {
          resolve(data) 
        },
        error: function(err) {
          reject(err) 
        }
      });
    });
  }

  
}

export default SpellCheckerService;