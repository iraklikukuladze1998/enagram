import $ from 'jquery';
import Config from "./config.json";

const serviceUrl = '/Common';

export class CommonService {

  changeToolsLanguage = (toolCode, toolsLangGlobalID, dialectCode) => {
    return new Promise(function(resolve, reject) {
      $.ajax({
        type: 'GET',
        url: Config.API_URL+serviceUrl+'/ChangeToolsLanguage?ToolsLangGlobalID='+toolsLangGlobalID+'&ToolCode='+toolCode+'&DialectCode='+dialectCode,
        dataType: 'json',
        async: false,
        contentType: "application/json; charset=utf-8",
        success: function(data) {
          resolve(data) 
        },
        error: function(err) {
          reject(err) 
        }
      });
    });
  }

  support = (request, webLanguege) => {
    return new Promise(function(resolve, reject) {
      $.ajax({
        type: 'POST',
        url: Config.API_URL + serviceUrl + '/Support',
        dataType: 'json',
        async: false,
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(request),
        headers: {'Accept-Language': webLanguege},
        success: function(data) {
          resolve(data) 
        },
        error: function(err) {
          reject(err) 
        }
      });
    });
  }

  contact = (request, webLanguege) => {
    return new Promise(function(resolve, reject) {
      $.ajax({
        type: 'POST',
        url: Config.API_URL + serviceUrl + '/Contact',
        dataType: 'json',
        async: false,
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(request),
        headers: {'Accept-Language': webLanguege},
        success: function(data) {
          resolve(data) 
        },
        error: function(err) {
          reject(err) 
        }
      });
    });
  }

  getStaticTexts = (webLanguege) => {
    return new Promise(function(resolve, reject) {
      $.ajax({
        type: 'GET',
        url: Config.API_URL+serviceUrl+'/GetStaticTexts',
        dataType: 'json',
        async: false,
        contentType: "application/json; charset=utf-8",
        headers: {'Accept-Language': webLanguege},
        success: function(data) {
          resolve(data) 
        },
        error: function(err) {
          reject(err) 
        }
      });
    });
  }

  getLicenses = (webLanguege) => {
    return new Promise(function(resolve, reject) {
      $.ajax({
        type: 'GET',
        url: Config.API_URL+serviceUrl+'/GetLicenses',
        dataType: 'json',
        async: false,
        contentType: "application/json; charset=utf-8",
        headers: {'Accept-Language': webLanguege},
        success: function(data) {
          resolve(data) 
        },
        error: function(err) {
          reject(err) 
        }
      });
    });
  }

  getStaticHTMLs = (webLanguege) => {
    return new Promise(function(resolve, reject) {
      $.ajax({
        type: 'GET',
        url: Config.API_URL+serviceUrl+'/GetStaticHTMLs',
        dataType: 'json',
        async: false,
        contentType: "application/json; charset=utf-8",
        headers: {'Accept-Language': webLanguege},
        success: function(data) {
          resolve(data) 
        },
        error: function(err) {
          reject(err) 
        }
      });
    });
  }

}