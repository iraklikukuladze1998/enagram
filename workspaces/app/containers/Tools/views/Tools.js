import React, { memo } from 'react'
import { useEffect, useMemo, useRef, useState } from "react";
import $ from 'jquery';
import { Editor } from '@tinymce/tinymce-react';
import useBus, { dispatch } from 'use-bus';
import SpellCheckerService from "./spellchecker.service";
import { CommonService } from "./common.service";
import Config from './config.json';
import Chrome from '../../imgs/icons/Chrome'
import ArrowDown from "../../imgs/icons/ArrowDown";
import Check from '../../imgs/icons/Check'
import Copy from '../../imgs/icons/Copy'
import Delete from '../../imgs/icons/Delete'
import Logo from '../../imgs/icons/Logo'
import Uk from '../../imgs/icons/UK'
import UKLogo from '../../imgs/icons/Uk.png'
import star from '../../LeftBar/views/upgrade.png'
import LeftBar from "../../LeftBar/views/LeftBar";
import { StyledHeader,StyledPremium,StyledDropDown,StyledRightWrapper, StyledNEwListWrapper,StyledNewMist, StyledMistakes, StyledSpeling, StyledDict, StyledConnect,StyledRightHead,StyledRighBarWrapper,StyledRighBar, StyledHeaderElements, StyledLeftBar, StyledMainWrapper,
    StyledActiveAngle ,StyledMainContent, StyledPageWrapper, StyledItemsTop, StyledItemsBottom,
    StyledNavBarItems, StyledNavBarLabel,StyledBorderWrapper, StyledBorder, StyledSwitcher,
    StyledSwWrapper, StyledChromeIcon,StyledUpgrade, StyledAvatarWrapper, StyledImg, StyledInputHeader, StyledCheckButton,
    StyledbuttonsWrapper, StyledInputButtons, StyledLanguage, StyledLineBetween, StyledInputWrapper, StyledInputFooter, StyledInput,
    StyledInputElement,StyledFooterIcons, StyledWords, StyledLogo, StyledRedLineWrapper
  }  from '../styled'

function Tools(props) {
    const toolCode = 'schunspell';
    const defaultActiveToolsLanguage = {
        Dialects: [{
            LangText: null,
            LangValue: "GEO"
        }],
        LangText: "ქართული",
        LangValue: 1
    }; 

    const activeRef = useRef(null);

    const darkSkinEditorContentStyle = 'body{background:#1C3549;color:white}';

    const accessToken = localStorage.getItem('token')


    const [activeToolsLanguage, setActiveToolsLanguage] = useState(defaultActiveToolsLanguage);
    const [downLanguageDropdown, setDownLanguageDropdown] = useState(undefined);
    const [toolsLanguages, setToolsLanguages] = useState([]);
    const [inncorectWords, setInncorectWords] = useState({});

    console.log(toolsLanguages,' lang')


    const [activeToolsLanguageDialect, setActiveToolsLanguageDialect] = useState(defaultActiveToolsLanguage.Dialects);
    const [downDialectDropdown, setDownDialectDropdown] = useState(undefined);

    const editorRef = useRef(null);
    const [disabledToolsButtons, setDisabledToolsButtons] = useState(true);

    const [suggestions, setSuggestions] = useState([]);
    const [suggestionWordData, setSuggestionWordData] = useState({});


    const [charactersPerText, setCharactersPerText] = useState(0);

    const [skinModeDarkStyles, setSkinModeDarkStyles] = useState((localStorage.getItem('EGSkinMode') === 'dark')?darkSkinEditorContentStyle:'');

    const [showEditor, setShowEditor] = useState(true);

    const [showFullscreen, setShowFullscreen] = useState(false);
    const [showFullscreenExitButton, setShowFullscreenExitButton] = useState(false);
    const [right, setRight] = useState({
        id: "",
        text: "",
      });


    useBus(
        'ToggleSkinMode',
        (data) => { 
            setSkinModeDarkStyles((data.SkinMode==='dark')?darkSkinEditorContentStyle:'');
            var content = editorRef.current.getContent();
            const t = 1000;
            setShowEditor(false);
            setTimeout(()=>{
                setShowEditor(true);
                setTimeout(()=>{
                    editorRef.current.setContent(content);
                }, t);
            }, t);
        }
    )

    useEffect(() => {
        getSpellCheckerLanguages();
    }, [skinModeDarkStyles, props.webLanguage]);

useEffect(() => {
    setErrorWords()
},[inncorectWords, suggestionWordData])

    useEffect(() => {
        if (sessionStorage.getItem("fullscreen", 1)) {
            $('.fullscreen-overlay').css('display', 'block');
            setShowFullscreen(false);
            $('.fullscreen-overlay').fadeOut(800);
            sessionStorage.removeItem("fullscreen");
            setShowFullscreenExitButton(true);
        }
    }, []);

    useMemo(()=>{
        if (sessionStorage.getItem("fullscreen", 1)) setShowFullscreen(true)
    }, []);
    const replaceWord = (word) => {
        var span = document.getElementById("word" + suggestionWordData.Id);
        span.innerText = word;
        $("#word" + suggestionWordData.Id).contents().unwrap(); 
        editorRef.current.setContent($("#taCheckResultHelper").html()); 
        fnCheckNext();
    }

    const fnCheckNext = () => {
        if ($("span:first", $('#taCheckResult')).length > 0) {
            $("span:first", $('#taCheckResult')).click();
        } else {
            $("#divCorrPane").removeClass('open');
            $('#mobileSuggestion').css('display', 'none')
            $('#divText').removeClass("col-lg-10 col-md-10").addClass("col-lg-12 col-md-12");
        }
    }

    const addToDictionary = () => {
        // const currentUser = JSON.parse(localStorage.getItem("user"));
        // if (!currentUser) {
        //     // const res = { 
        //     //     Status: 1, 
        //     //     Message: 'გთხოვთ, გაიაროთ ავტორიზაცია' 
        //     // }
        //     displayMessageOfAddToDictionary(res);
        //     $('.rightbar__left__nav__li.rs-profile a').click();
        //     return;
        // }
        if (!suggestionWordData.Word) return;
        const request = {
            "Word": suggestionWordData.Word,
            "Lang": "GEO"
        }
        new SpellCheckerService().addToDictionary(accessToken, request, props.webLanguage).then(function(response) { 
            if (response.Success) {
                $(".bg-color-fade").each(function () {
                    if ($(this).text() === suggestionWordData.Word) $(this).contents().unwrap();
                });
                editorRef.current.setContent($("#taCheckResultHelper").html()); 
                fnCheckNext();
                displayMessageOfAddToDictionary(response);
                dispatch('GetDictionary');
            } else { 
                const res = { 
                    Status: 1, 
                    Message: response.Message 
                }
                displayMessageOfAddToDictionary(res);
            }
        }).catch(function(err) {
            console.log(err);
            // alert('Error'); 
            if (!err.responseJSON) {
                alert('Error');
                return;
            }
            const res = { 
                Status: 1, 
                Message: err.responseJSON.Message 
            }
            displayMessageOfAddToDictionary(res);
        });
    }

    const displayMessageOfAddToDictionary = (result) => {
        $('.sch__add-to-dictionary-message').html('');
        if (!result) return;
        let color = '#2770de';
        if (result.Status === 1) color = 'red';
        $('.sch__add-to-dictionary-message').html(`<span style="color: ${color}">${result.Message}</span>`);
        setTimeout(() => {
            $('.sch__add-to-dictionary-message').html('');
        }, 3000);
    }

    const ignoreWord = () => {
        $('#word' + suggestionWordData.Id).contents().unwrap();
        editorRef.current.setContent($("#taCheckResultHelper").html()); 
        fnCheckNext();
    }

    const ignoreAllWord = () => {
        $(".bg-color-fade").each(function () {
            if ($(this).text() === suggestionWordData.Word) {
                $(this).contents().unwrap();
            }
        });
        editorRef.current.setContent($("#taCheckResultHelper").html()); 
        fnCheckNext();
    }

    const editorOnKeyUp = (event, editor) => {
        const charactersLength = editor.getContent({ format: "text" }).length;
        if (charactersLength === 0) 
            $('#addToDictionary,#ignore,#ignoreAll').addClass('custom-disabled');
        if (charactersLength <= charactersPerText) {
            $('.sc__characters-count span:eq(0)').text(charactersLength);
            $('.sc__characters-count').removeClass('text-danger');
        } else {
            $('.sc__characters-count span:eq(0)').text(charactersLength);
            // $('.sc__characters-count span:eq(1)').text(charactersPerText);
            $('.sc__characters-count').addClass('text-danger');
        }   
    }

    const setErrorWords = () => {
        let arg =[]
        var all = $(".bg-color-fade").map(function() {
             arg.push({ id : $(this).attr('data-id'), text:  $(this).text()})
        }
        );
        setRight(arg)
    }

    const getSpellCheckerLanguages = () => {
        const toolsLangGlobalID = defaultActiveToolsLanguage.LangValue; 
        // const currentUser = JSON.parse(localStorage.getItem("user"));
        new SpellCheckerService().getSpellCheckerLanguages(toolsLangGlobalID, props.webLanguage, accessToken).then(function(response) { 
            if (!response.Success) {
                alert('Error load tools languages');
                return;
            }
            setToolsLanguages(response.ToolsLanguages);
            setCharactersPerText(response.CharactersPerText);
            
            const atl = activeToolsLanguage;
            atl.LangText = response.ToolsLanguages.find(l=>l.LangValue==='1').LangText;
            atl.Dialects = defaultActiveToolsLanguage.Dialects;
            setActiveToolsLanguage(atl);
            $('#myInput').val('');
            filterToolsLanguages();
        }).catch(function(err) {
            alert('Error');
        });
    }

    const checkSpelling = () => {
        if (!editorRef.current) return;
        var textContent = editorRef.current.getContent({ format: "text" });
        if (textContent.length <= 500000) {
            if (textContent.length > 0) setDisabledToolsButtons(false);
            // TODO dialect
            // if ($('#divDialect').is(":visible")) {
            //     lang = select_dialect.value;
            // }
            // const currentUser = JSON.parse(localStorage.getItem("user"));
            const request = {
                "Text": textContent,
                "ToolsLangCode": 'GEO',
                "ToolsLangGlobalID": defaultActiveToolsLanguage.LangValue
            }
            new SpellCheckerService().checkWord(accessToken, request, props.webLanguage).then(function(response) {
                if (!response.Success) {
                    alert('Error');
                    return;
                }
                if (response.length === 0) return;
                if (response.lstWR[0].Status === 2) window.location.reload();
                if (response.length > 0 && response.lstWR[0].Status === 1) {
                    var res = {
                        Status: 1,
                        Message: 'შეყვანილი ტექსტი ძალიან გრძელია'
                    };
                    displayMessageOfAddToDictionary(res);
                } else {
                    let words = [];
                    var errorCount = 0;
                    const lstWR = response.lstWR;
                    for (var prop in lstWR) {
                        if (lstWR[prop]) {
                            var item = lstWR[prop];
                            if (item.Correct !== 0) {
                                errorCount++;                                    
                                words.push(item.Word);                                        
                            }
                        }
                    }
                    let reg = new RegExp("("+words.join("|")+")[s]?", "gi");
                    if (errorCount === 0) {
                        $('.sch__add-to-dictionary-message').html('<span style="color: #2770de;">ტექსტი უშეცდომოა</span>');
                        setTimeout(() => {
                            $('.sch__add-to-dictionary-message').html('');
                        }, 3000);
                    }
                    let content = editorRef.current.getContent();

                    /* Clear highlighted words */
                    document.getElementById('taCheckResultHelper').innerHTML = content;
                    $("#taCheckResultHelper").find('.bg-color-fade').each(function(i, obj){
                        while(obj.attributes.length > 0){
                            obj.removeAttribute(obj.attributes[0].name);
                        }
                    });
                    editorRef.current.setContent($("#taCheckResultHelper").html()); 
                    /* End */
                    
                    content = editorRef.current.getContent();    
                    let v = content;
                    if (words.length !== 0) {
                        setInncorectWords(words)
                        v = content.replace(reg, (word, i, j) => `<span id="word${j}" class="bg-color-fade" data-id="${j}" style="border-bottom:solid 3px red">${word}</span>`);  
                    }
                    editorRef.current.setContent(v) 
                    $("#taCheckResultHelper").html(v);
                }
            }).catch(function(err){
                alert('Error2');
            });
        } else {
            var res = {
                Status: 1,
                Message: 'შეყვანილი ტექსტი ძალიან გრძელია'
            };
            displayMessageOfAddToDictionary(res);
        }
    }

    const changeToolsLanguage = (language) => {
        setActiveToolsLanguage(language);
        setActiveToolsLanguageDialect(language.Dialects[0]);
        new CommonService().changeToolsLanguage(toolCode, activeToolsLanguage.LangValue, activeToolsLanguage.Dialects[0].LangValue).then(function(response) {
            if (!response.Success) {
                alert('Error');
                return;
            }
            setDownLanguageDropdown(false);
        }).catch(function(err) {
            alert('Error');
        });
    }

    const changeToolsLanguageDialect = (dialect) => {
        //  todo  ?? ChangeToolsLanguage does call?
        setActiveToolsLanguageDialect(dialect);
        setDownDialectDropdown(false);
    }

    const filterToolsLanguages = () => {
        var input, filter, ul, li, i, txtValue;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        ul = document.querySelector(".tabsLangSelectInner");
        li = ul.getElementsByTagName("a");
        for (i = 0; i < li.length; i++) {
            txtValue = li[i].textContent || li[i].innerText;
            li[i].style.display = (txtValue.toUpperCase().indexOf(filter) > -1) ? "" : "none";
        }
    }

    const editorOnClick = (event, editor) => {
        if ($(event.target).attr('class') === 'bg-color-fade') getSuggestions(null, $(event.target).attr('data-id'), $(event.target).text());
    }

    const getSuggestions = (e, id, word2) => {
        const toolsLangCode = 'GEO';
        new SpellCheckerService().getSuggestions(accessToken ,word2, toolsLangCode).then(function(response) {
            if (!response.Success) {
                alert('Error');
                return
            }
            var wcount = 0;
            setSuggestions(response.lstWords);
            setSuggestionWordData({'Id': id, 'Word': word2});
            $('#ltrWordCount').text(wcount++);
            $("#divCorrPane").addClass('open');
            // $('#divSuggestions').html(contextMenu);
            $('#mobileSuggestion').css('display', 'flex')
            // $('#mobileSuggestion').children('.words').html(contextMenu)
            $('#divWord').html($('#word' + id).text());
            $(".custom-menu").show();
        }).catch(function() {
            alert('Error');
        });
    }


    return (
         <StyledPageWrapper>
         <StyledMainWrapper>
           <LeftBar />
           <StyledMainContent>
            <StyledPremium>
                <div> <img src={star} />Upgrade to Premium</div>
            </StyledPremium>
             <StyledInputHeader>
               <StyledbuttonsWrapper>
                 <StyledCheckButton onClick={()=>checkSpelling()}>   <Check /> Check</StyledCheckButton>
               </StyledbuttonsWrapper>
               <StyledbuttonsWrapper>
               <StyledInputButtons > <input type='checkBox' /> Keep format  </StyledInputButtons>
                 <StyledInputButtons> <Copy /> Copy  </StyledInputButtons>
                 <StyledInputButtons> <Delete /> Delete  </StyledInputButtons>
                 <StyledLineBetween />
                 <StyledLanguage>
                 <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M12.989 2C6.917 2 2 6.928 2 13C2 19.072 6.917 24 12.989 24C19.072 24 24 19.072 24 13C24 6.928 19.072 2 12.989 2ZM20.612 8.6H17.367C17.015 7.225 16.509 5.905 15.849 4.684C17.873 5.377 19.556 6.785 20.612 8.6ZM13 4.244C13.913 5.564 14.628 7.027 15.101 8.6H10.899C11.372 7.027 12.087 5.564 13 4.244ZM4.486 15.2C4.31 14.496 4.2 13.759 4.2 13C4.2 12.241 4.31 11.504 4.486 10.8H8.204C8.116 11.526 8.05 12.252 8.05 13C8.05 13.748 8.116 14.474 8.204 15.2H4.486ZM5.388 17.4H8.633C8.985 18.775 9.491 20.095 10.151 21.316C8.127 20.623 6.444 19.226 5.388 17.4ZM8.633 8.6H5.388C6.444 6.774 8.127 5.377 10.151 4.684C9.491 5.905 8.985 7.225 8.633 8.6ZM13 21.756C12.087 20.436 11.372 18.973 10.899 17.4H15.101C14.628 18.973 13.913 20.436 13 21.756ZM15.574 15.2H10.426C10.327 14.474 10.25 13.748 10.25 13C10.25 12.252 10.327 11.515 10.426 10.8H15.574C15.673 11.515 15.75 12.252 15.75 13C15.75 13.748 15.673 14.474 15.574 15.2ZM15.849 21.316C16.509 20.095 17.015 18.775 17.367 17.4H20.612C19.556 19.215 17.873 20.623 15.849 21.316ZM17.796 15.2C17.884 14.474 17.95 13.748 17.95 13C17.95 12.252 17.884 11.526 17.796 10.8H21.514C21.69 11.504 21.8 12.241 21.8 13C21.8 13.759 21.69 14.496 21.514 15.2H17.796Z" fill="white" stroke="#2D62ED" stroke-width="0.4"/>
</svg>
                   Georgian
                   <ArrowDown/>
                   <StyledDropDown>
                    {
                        toolsLanguages.map((item) => (
                            <div>{item.LangText}</div>
                        ))
                    }

                   </StyledDropDown>
                 </StyledLanguage>
               </StyledbuttonsWrapper>
             </StyledInputHeader>
             <StyledInputWrapper>
             <div className="re-components-container">


<main>
    
    {/* <label className="d-none" id="toolongmessage">შეყვანილი ტექსტი ძალიან გრძელია</label>
    <label className="d-none" id="typesometext">შეიყვანეთ ტექსტი</label>
    <label className="d-none" id="filetoolarge">ფაილი ძალიან დიდია</label>
    <label className="d-none" id="uploadfile">გთხოვთ ატვირთოთ ფაილი</label> */}


    <section className="section__container">

        {/* @RenderBody() */}

        {/* <div className="tools__unauthenticated-overlay"></div> */}
        <div className="scrollable-content">
            <div>

                <div className="container-text-box-container">
                    
                    { 
                        (showEditor) ? (
                            <Editor
                                apiKey={ Config.TINYMCE_API_KEY }
                                onInit={(evt, editor) => editorRef.current = editor}
                                onClick={ (evt, editor) => editorOnClick(evt, editor) }
                                onKeyUp={ (evt, editor) => editorOnKeyUp(evt, editor) }
                                // initialValue="<p>თბბილისი</p>"
                                init={{
                                    height: 800,
                                    menubar: false,
                                    plugins: [
                                        'advlist autolink lists link image charmap print preview anchor',
                                        'searchreplace visualblocks code fullscreen',
                                        'insertdatetime media table paste code help wordcount'
                                    ],
                                    toolbar: ' formatselect | ' +
                                    'bold italic | alignleft aligncenter ' +
                                    'alignright alignjustify | bullist numlist outdent indent | ' +
                                    'removeformat',
                                    content_style: `body { font-family:Helvetica,Arial,sans-serif; font-size:14px } ${skinModeDarkStyles}`
                                }}
                            />
                        ) : (
                            <div className="editor-loading"><div>loading..</div></div>
                        )
                    }

                    <div id="taCheckResultHelper" style={{'display':'none'}}></div>
                    <div className="sc__characters-count"><span>0</span>{" "}<span>characters</span></div>
                </div>
            </div>
        </div>
    </section>

</main>

</div>
             </StyledInputWrapper>
           </StyledMainContent>
           <StyledRighBar>
            <StyledRighBarWrapper>
        <StyledRightHead>
            <StyledConnect>
                <span>connections</span>
                <div>23</div>
            </StyledConnect>

            <div>Dictionary</div>
        </StyledRightHead>
        <StyledRightWrapper>
        { right.id !== '' && right.map((item) => (
            <StyledNEwListWrapper >
                {  suggestionWordData.Id !== item.id &&
                <StyledNewMist onClick={() => getSuggestions(null, item.id, item.text)}>
                       <div>
                       <svg width="10" height="10" viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                               <rect width="10" height="10" rx="5" fill="#DB5E5E"/>
                               </svg>
                           <span>{item.text}</span>
                       </div>
                       <div>
                         <svg width="24" height="34" viewBox="0 0 24 34" fill="none" xmlns="http://www.w3.org/2000/svg">
                           <g clip-path="url(#clip0_679_863)">
                           <path d="M12.0001 11.828L9.17208 14.657L7.75708 13.243L12.0001 9L16.2431 13.243L14.8281 14.657L12.0001 11.828Z" fill="#959595"/>
                           </g>
                           <path d="M11.9999 22.172L14.8279 19.343L16.2429 20.757L11.9999 25L7.75692 20.757L9.17192 19.343L11.9999 22.172Z" fill="#959595"/>
                           <defs>
                           <clipPath id="clip0_679_863">
                           <rect width="24" height="24" fill="white"/>
                           </clipPath>
                           </defs>
                           </svg>
           
                       </div>
                       
                </StyledNewMist>
}
                {
            suggestionWordData.Id  && suggestionWordData.Id === item.id &&
        
        <StyledDict>
            <StyledSpeling>       
                 <svg width="10" height="10" viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect width="10" height="10" rx="5" fill="#DB5E5E"/>
</svg>spelling mistake</StyledSpeling>
<StyledMistakes>
<div id="divCorrPane" className="mistakes-box flex-box column">
                    <div className="wrong-word flex-box al-center ">
                        <div id="divWord" className="word-box"></div> <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M16.172 11L10.808 5.63598L12.222 4.22198L20 12L12.222 19.778L10.808 18.364L16.172 13H4V11H16.172Z" fill="#44474D"/>
</svg>

                    </div>
                    <div className="suggestions-box-container">
                        <div className="suggestions-box">
                            <div id="divSuggestions">
                                <ul className="suggestions">
                                    { 
                                        (suggestions.length>0) ? 
                                            (
                                                suggestions.map((word, index) => {
                                                    return <li key={index} data-action="first" onClick={()=>replaceWord(word)}> { word }</li>
                                                })
                                            ) : 
                                            (
                                                <div></div>
                                            )
                                    }
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
</StyledMistakes>
<div className='dismissButtons'>
    <div  onClick={ () => addToDictionary() }>
    <svg width="20" height="22" viewBox="0 0 20 22" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M18 2H4C3.46957 2 2.96086 2.21071 2.58579 2.58579C2.21071 2.96086 2 3.46957 2 4C2 4.53043 2.21071 5.03914 2.58579 5.41421C2.96086 5.78929 3.46957 6 4 6H18V19C18 19.2652 17.8946 19.5196 17.7071 19.7071C17.5196 19.8946 17.2652 20 17 20H4C2.93913 20 1.92172 19.5786 1.17157 18.8284C0.421427 18.0783 0 17.0609 0 16V4C0 2.93913 0.421427 1.92172 1.17157 1.17157C1.92172 0.421427 2.93913 0 4 0H17C17.2652 0 17.5196 0.105357 17.7071 0.292893C17.8946 0.48043 18 0.734784 18 1V2ZM2 16C2 16.5304 2.21071 17.0391 2.58579 17.4142C2.96086 17.7893 3.46957 18 4 18H16V8H4C3.29782 8.00112 2.60784 7.81655 2 7.465V16ZM17 5H4C3.73478 5 3.48043 4.89464 3.29289 4.70711C3.10536 4.51957 3 4.26522 3 4C3 3.73478 3.10536 3.48043 3.29289 3.29289C3.48043 3.10536 3.73478 3 4 3H17V5Z" fill="#747474"/>
<line x1="17" y1="14" x2="17" y2="20" stroke="#F8F8F8" stroke-width="2"/>
<line x1="17" y1="19" x2="12" y2="19" stroke="#F8F8F8" stroke-width="2"/>
<line x1="16.9999" y1="16" x2="16.9999" y2="22" stroke="#747474" stroke-width="1.2"/>
<line x1="20" y1="19" x2="14" y2="19" stroke="#747474" stroke-width="1.2"/>
</svg>

        <span>Add to your Dictionary</span>
    </div>
    <div onClick={ () => ignoreWord() }>
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M4 8H20V21C20 21.2652 19.8946 21.5196 19.7071 21.7071C19.5196 21.8946 19.2652 22 19 22H5C4.73478 22 4.48043 21.8946 4.29289 21.7071C4.10536 21.5196 4 21.2652 4 21V8ZM6 10V20H18V10H6ZM9 12H11V18H9V12ZM13 12H15V18H13V12ZM7 5V3C7 2.73478 7.10536 2.48043 7.29289 2.29289C7.48043 2.10536 7.73478 2 8 2H16C16.2652 2 16.5196 2.10536 16.7071 2.29289C16.8946 2.48043 17 2.73478 17 3V5H22V7H2V5H7ZM9 4V5H15V4H9Z" fill="#747474"/>
</svg>

        <span>Dismiss</span>
    </div>
</div>
        </StyledDict>
}
                </StyledNEwListWrapper>
      ))}
      </StyledRightWrapper>

            </StyledRighBarWrapper>
           </StyledRighBar>
         </StyledMainWrapper>
       </StyledPageWrapper>
    );
}

export default memo(Tools)