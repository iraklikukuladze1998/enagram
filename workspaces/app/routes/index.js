import { Home } from 'containers/Home'
import { NotFound } from 'containers/NotFound'
import { Business } from 'containers/Business'
import { Tools } from 'containers/Tools'

export const routeNameMap = {
  HOME: '/',
  BUSINESS: '/Business',
  TOOLS: '/Tools',
}

export const routes = [
  {
    path: routeNameMap.HOME,
    exact: true,
    component: () =>  Home,
  },
  {
    path: routeNameMap.BUSINESS,
    exact: true,
    component: () => Business,
  },
  {
    path: routeNameMap.TOOLS,
    exact: true,
    component: () => Tools,
  },
  {
    component: () => NotFound,
    isNotFound: true,
  },
]
